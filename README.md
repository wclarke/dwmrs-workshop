# FSL at Best Practices & Tools for Diffusion MR Spectroscopy
**Lorentz Center, 20 - 24 September 2021**

This repository contains [FSL-MRS](https://git.fmrib.ox.ac.uk/fsl/fsl_mrs) code for the [diffusion MRS workshop](https://www.lorentzcenter.nl/best-practices-en-tools-for-diffusion-mr-spectroscopy.html) taking place at the Lorentz Centre, Leiden (and online) in September 2021.

## Contents
The repository contains:
1. Three guides in Python notebook format, showing the user how to analyse the `pregame` dMRS data supplied by the workshop organisers.
2. A folder concerning basis spectra generation for the STEAM and sLASER sequences used to acquire the `pregame` data.
3. A copy of the `pregame` data repository.

## Google colab notebooks
The three jupyter notebooks are hosted online as Google Colabs:
- [Synthetic Data](https://colab.research.google.com/drive/1ZYbKBCE_t_-PsXh674zwzITWCEDgmc_w?usp=sharing)
- [In vivo 1](https://colab.research.google.com/drive/1FKA9NCMcTzp54spLhAw6nqkPSh01tu3u?usp=sharing)
- [In vivo 2](https://colab.research.google.com/drive/12y-bQVGJO_ZOwcDtMGbds3ULnoYUMWkC?usp=sharing)

## Installing FSL-MRS
Instructions for installing FSL-MRS can be found online [here](https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/install.html).

## FSL-MRS guide
Please see the [online documentation](https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/index.html) and associated [quick-start guide](https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/quick_start.html) for FSL-MRS.

## Contact
Please contact the developers directly, or alternatively post at [FSL@JISCMAIL.AC.UK](mailto:fsl@jiscmail.ac.uk).
